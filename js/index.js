window.onload = main;

var caroucelAC = null;
var caroucelACN = null;
var els = null;

function main() {
    var elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }

    // Camoufle les carousel
    showView("fPage");


    document.getElementById('poidsValeur').addEventListener('change', calculAvecPoids);

    // Create event listener for the main timer 
    createListenerButtonChrono(startChrono, startCount);
    createListenerButtonChrono(resetChrono, resetCount);
    createListenerButtonChrono(stopChrono, stopCount);

    document.getElementById('poidsValeur').addEventListener('input', calculAvecPoids);
    document.getElementById('ageValeurMois').addEventListener('input', calculAvecAge);
    document.getElementById('ageValeurAnne').addEventListener('input', calculAvecAge);

    var constantes = document.getElementById('constantes');
    constantes.addEventListener('click', function () {

        clearTimeout(generalTimer);
        clearTimeout(timer);
        clearTimeout(timer2);
        clearTimeout(timer3);

        hideEclair();
        hideAdre();
        showView('fPage');
    });

    var AC = document.getElementById('arretDef');
    AC.addEventListener('click', acCarousel);

    var ACN = document.getElementById('arretNonDef');
    ACN.addEventListener('click', acnCarousel);


    var els = document.getElementsByClassName('carousel');
    for (var i = 0; i < els.length; i++) {
        els[i].style.height = window.innerHeight * 0.85 + 'px';
    }

    var AVP = document.getElementById('trauma');
    AVP.addEventListener('click', function () {
        clearTimeout(timer);
        clearTimeout(timer2);
        clearTimeout(timer3);
        clearTimeout(generalTimer);
        hideEclair();
        hideAdre();
        showView('traumaCranienAVP');
    });

    var desat = document.getElementById('desat');
    desat.addEventListener('click', function () {
        clearTimeout(timer);
        clearTimeout(timer2);
        clearTimeout(timer3);
        clearTimeout(generalTimer);
        hideEclair();
        hideAdre();
        showView('desaturation');
    });

    var tsvLink = document.getElementById('tsvLink');
    tsvLink.addEventListener('click', function () {
        clearTimeout(timer);
        clearTimeout(timer2);
        clearTimeout(timer3);
        clearTimeout(generalTimer);
        hideEclair();
        hideAdre();
        showView('tsv');
    });

    var elems = document.querySelectorAll('.tooltipped');
    M.Tooltip.init(elems);
}


// "Onglets"
function showView(viewId) {
    var views = document.getElementsByClassName('view');
    for (var i = 0; i < views.length; i++) {
        views[i].style.display = "none";
    }
    var element = document.getElementById(viewId);
    element.style.display = "block";
}

//Variable timer
var c = 0;
var t;
var timer_is_on = 0;

function toStringChrono(c) {
    var sec_num = parseInt(c, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
}

function timedCount() {
    txt.innerHTML = toStringChrono(c);
    c = c + 1;
    t = setTimeout(timedCount, 1000);
}

function startCount() {
    if (!timer_is_on) {
        timer_is_on = 1;
        timedCount();
    }
}

function stopCount() {
    clearTimeout(t);
    timer_is_on = 0;
}

function resetCount() {
    txt.innerHTML = toStringChrono(0);
    clearTimeout(t);
    c = 0;
    timer_is_on = 0;
}




function createListenerButtonChrono(id, nomFonction) {
    id.addEventListener("click", function () {
        nomFonction();
    });
}

function showMore(args) {
    for (var i = 0; i < args.length; i++) {
        args[i].style.display = "block";
    }
}