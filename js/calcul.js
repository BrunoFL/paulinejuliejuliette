console.log('calcul');
function calculAvecPoids() {
    var poids = parseFloat(document.getElementById('poidsValeur').value); //kg
    if (Number.isNaN(poids)) {
        document.getElementById('calculRemplissage').innerHTML = 'Remplir le poids';
        document.getElementById('calculAdre').innerHTML = 'Remplir le poids';
        return;
    }

    var remplissage = 20 * poids;
    remplissage = (remplissage > 500) ? 500 : remplissage; //ml
    document.getElementById('calculRemplissage').innerHTML = remplissage.toFixed(0) + ' ml';

    var adrenaline = 0.1 * poids; //ml
    document.getElementById('calculAdre').innerHTML = adrenaline.toFixed(1) + ' ml';
    var els = document.getElementsByClassName('adreAC');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = adrenaline.toFixed(1) + ' ml';
    }

    var mannitol = 0.5 * poids; //g
    var els = document.getElementsByClassName('calculMannitol');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = mannitol.toFixed(2) + ' g';
    }

    var cardioVersion = 1 * poids; //Joule
    //Si besoin de renouveller 2J / kg
    var els = document.getElementsByClassName('calculCV1');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = cardioVersion + ' J';
    }
    var els = document.getElementsByClassName('calculCV2');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = (cardioVersion * 2) + ' J';
    }

    var adenosine = 0.1 * poids;
    adenosine = (adenosine > 12) ? 12 : adenosine;
    var els = document.getElementsByClassName('calculAdenosine1');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = adenosine.toFixed(1) + ' mg';
    }
    var adenosine = 0.2 * poids;
    adenosine = (adenosine > 24) ? 24 : adenosine;
    var els = document.getElementsByClassName('calculAdenosine2');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = adenosine.toFixed(1) + ' mg';
    }

    var defibrilation = 4 * poids; // Joule
    var els = document.getElementsByClassName('cee1');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = defibrilation + 'J';
    }

    var serumSaleHyperTonique3Percent = 3 * poids; //ml
    var els = document.getElementsByClassName('calculSSH');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = serumSaleHyperTonique3Percent.toFixed(2) + ' ml';
    }

    //En cas d arret cardiaque
    var amiodarone = 5 * poids; //mg
    var els = document.getElementsByClassName('amioAC');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = amiodarone + ' mg';
    }

    var forceReaDobutamine = poids / 3;
    document.getElementById('calculForceReaAdre').innerHTML = forceReaDobutamine.toFixed(1) + ' ml/H';
}

function calculAvecAge() {
    var mois = parseInt(document.getElementById('ageValeurMois').value); //kg
    var anne = parseInt(document.getElementById('ageValeurAnne').value);
    if (Number.isNaN(anne)) {
        document.getElementById('ageValeurAnne').value = 0;
        anne = 0;
        document.getElementById('poidsValeur').value = null;
    } else if (anne >= 1 && anne <= 11) {
        document.getElementById('poidsValeur').value = (anne + 4) * 2;
        document.getElementById('ageValeurMois').value = 0;
    } else if (anne >= 2) {
        document.getElementById('ageValeurMois').value = 0;
        document.getElementById('poidsValeur').value = null;
        mois = 0;
    }
    calculAvecPoids();

    var respir = getRespiratoryRate(anne, mois);
    var hearth = getHeartRate(anne, mois);

    document.getElementById('calculFCLow').innerHTML = hearth.low;
    document.getElementById('calculFCHigh').innerHTML = hearth.high;
    document.getElementById('calculFRLow').innerHTML = respir.low;
    document.getElementById('calculFRHigh').innerHTML = respir.high;

    var tas = bloodPressureIsOk(anne, mois, true);
    document.getElementById('calculTAS').innerHTML = tas;

    var tam = bloodPressureIsOk(anne, mois, false);
    // arrondi en dessous 
    tam = Math.floor(tam);
    document.getElementById('calculTAM').innerHTML = tam;

    var taille;
    if (anne == 0) {
        if (mois <= 1) {
            taille = 3.5;
        } else {
            taille = 4
        }
    } else if (anne == 1 || anne == 2) {
        taille = 4.5;
    } else {
        taille = (anne / 4) + 4;
    }
    document.getElementById('calculTailleSIT').innerHTML = (taille - 0.5) + ' - <span class="gros">' + taille + '</span> - ' + (taille + 0.5);
    document.getElementById('calculRepereSIT').innerHTML = taille * 3;


    var poids = parseFloat(document.getElementById('poidsValeur').value)
    var exacyl = (anne <= 10) ? (10 * poids) : 1000;
    exacyl = (exacyl > 1000) ? 1000 : exacyl;
    var els = document.getElementsByClassName('exacyl');
    for (var i = 0; i < els.length; i++) {
        els[i].innerHTML = exacyl + 'mg';
    }
}

function getRespiratoryRate(year, month) {
    if (year < 0 || month < 0 || month > 12) {
        return { low: -1, high: -1 };
    }

    if (year == 0) {
        if (month == 0 || month == 1) {
            return { low: 30, high: 50 };
        } else if (month <= 6) {
            return { low: 20, high: 40 };
        } else {
            return { low: 20, high: 30 };
        }
    } else if (year == 1 || year == 2) {
        return { low: 20, high: 30 };
    } else if (year <= 12) {
        return { low: 16, high: 24 };
    } else {
        return { low: 12, high: 25 };
    }
}



function getHeartRate(year, month) {
    if (year < 0 || month < 0 || month > 12) {
        return { low: -1, high: -1 };
    }

    if (year == 0) {
        if (month <= 6) {
            return { low: 100, high: 170 };
        } else {
            return { low: 90, high: 150 };
        }
    } else if (year == 1 || year == 2) {
        return { low: 90, high: 150 };
    } else if (year == 3 || year == 4) {
        return { low: 80, high: 140 };
    } else if (year <= 10) {
        return { low: 70, high: 130 };
    } else if (year <= 14) {
        return { low: 60, high: 125 };
    } else {
        return { low: 60, high: 110 };
    }
}

function bloodPressureIsOk(year, month, systolicPressure) {
    if (year < 0 || month < 0 || month > 12) {
        return -1;
    }

    if (year == 0) {
        if (month == 0 || month == 1) {
            return (systolicPressure) ? 50 : 35;
        } else {
            return (systolicPressure) ? 70 : 40;
        }
    } else if (year <= 9) {
        return (systolicPressure) ? (70 + 2 * year) : (40 + 1.5 * year);
    } else if (year < 12) {
        return (systolicPressure) ? 90 : (40 + 1.5 * year);
    } else {
        return (systolicPressure) ? 90 : 65;
    }
}
